<?php

namespace Drupal\flexible_field_wrappers\EventSubscriber;

use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\Plugin\Block\FieldBlock;
use Drupal\flexible_field_wrappers\FieldWrapper;

/**
 * Class BlockComponentRenderArraySubscriber.
 */
class BlockComponentRenderArraySubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Layout Builder also subscribes to this event to build the initial render
    // array. We use a higher weight so that we execute after it.
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = ['onBuildRender', 50];
    return $events;
  }

  /**
   * Add each component's block styles to the render array.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $build = $event->getBuild();
    // This shouldn't happen - Layout Builder should have already created the
    // initial build data.
    if (empty($build)) {
      return;
    }

    $component = $event->getComponent();
    $plugin = $event->getPlugin();

    if ($plugin instanceof FieldBlock && !$event->inPreview()) {
      $display_settings = $plugin->getConfiguration()['formatter'];
      if (isset($display_settings['third_party_settings']['flexible_field_wrappers'])) {
        $settings = $display_settings['third_party_settings']['flexible_field_wrappers'];
        FieldWrapper::applyWrappersToField($build['content'], $settings, $plugin->getContextValue('entity'));
        $event->setBuild($build);
      }
    }
    
  }

}
