<?php

namespace Drupal\flexible_field_wrappers;


use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Render\Element;
use Drupal\Core\Entity\EntityInterface;

class FieldWrapper {
  public static function settingsForm(FormatterInterface $plugin, FieldDefinitionInterface $field_definition, $view_mode, $form, FormStateInterface $form_state) {
    $elements = [];

    $field_info = $field_definition->getFieldStorageDefinition();
    if (!empty($field_info)) {
      $token_types = [$field_definition->getTargetEntityTypeId()];

      $field_name = $field_info->getName();
      $wrapper_options = ['div'];
      $wrapper_options = ['none' => t('None')] + array_combine($wrapper_options, $wrapper_options) + ['other' => t('Other')];

      $elements['field_wrappers'] = [
        '#type' => 'details',
        '#title' => t('Wrapper settings'),
        // Default to open if we have any values to show.
        '#open' => $settings['label_class']['value'] || $settings['label_value']['value'] || $settings['label_tag']['value'],
      ];

      $elements['field_wrappers']['field_wrapper'] = [
        '#type' => 'select',
        '#title' => t('Field wrapper'),
        '#description' => t('Select a wrapper element to be used for entire field output.'),
        '#default_value' => $plugin->getThirdPartySetting('flexible_field_wrappers', 'field_wrapper'),
        '#options' => $wrapper_options,
      ];

      $elements['field_wrappers']['field_wrapper_other'] = [
        '#type' => 'textfield',
        '#description' => t('Enter HTML tag name of the wrapper element.'),
        '#default_value' => $plugin->getThirdPartySetting('flexible_field_wrappers', 'field_wrapper_other'),
        '#element_validate' => [ [self::class, 'validateHtmlTagName'] ],
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $field_name . '][settings_edit_form][third_party_settings][flexible_field_wrappers][field_wrapper]"]' => ['value' => 'other'],
          ],
        ],
        '#depended_field' => 'field_wrapper'
      ];

      $elements['field_wrappers']['field_wrapper_classes'] = [
        '#type' => 'textfield',
        '#title' => t('Field wrapper HTML classes'),
        '#description' => t('Enter one or more HTML class names separated by spaces. It can be used to style the field with CSS.'),
        '#default_value' => $plugin->getThirdPartySetting('flexible_field_wrappers', 'field_wrapper_classes'),
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $field_name . '][settings_edit_form][third_party_settings][flexible_field_wrappers][field_wrapper]"]' => ['!value' => 'none'],
          ],
        ],
      ];

      $elements['field_wrappers']['field_wrapper_attributes'] = [
        '#title' => t('Field wrapper attributes'),
        '#type' => 'textarea',
        '#description' => t('Set attributes for this wrapper. Enter one value per line, in the format attribute|value. The value is optional. Tokens are supported in attribute values.'),
        '#default_value' => $plugin->getThirdPartySetting('flexible_field_wrappers', 'field_wrapper_attributes'),
        '#element_validate' => [ [self::class, 'validateHtmlAttributesList'] ],
        '#depended_field' => 'field_wrapper',
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $field_name . '][settings_edit_form][third_party_settings][flexible_field_wrappers][field_wrapper]"]' => ['!value' => 'none'],
          ],
        ],
      ];

      if (\Drupal::moduleHandler()->moduleExists('token')) {
        $elements['field_wrappers']['field_wrapper_attributes_token'] = [
          '#type' => 'item',
          '#theme' => 'token_tree_link',
          '#token_types' => $token_types,
          '#states' => [
            'visible' => [
              ':input[name="fields[' . $field_name . '][settings_edit_form][third_party_settings][flexible_field_wrappers][field_wrapper]"]' => ['!value' => 'none'],
            ],
          ],
        ];
      }

      $item_wrapper_options = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'div', 'span', 'p', 'strong'];
      $item_wrapper_options = ['none' => t('None')] + ['link' => t('Link')] + array_combine($item_wrapper_options, $item_wrapper_options) + ['other' => t('Other')];

      $elements['field_wrappers']['field_item_wrapper'] = [
        '#type' => 'select',
        '#title' => t('Field item wrapper'),
        '#description' => t('Select a wrapper element to be used for each field item value output.'),
        '#default_value' => $plugin->getThirdPartySetting('flexible_field_wrappers', 'field_item_wrapper'),
        '#options' => $item_wrapper_options,
      ];

      $elements['field_wrappers']['field_item_wrapper_other'] = [
        '#type' => 'textfield',
        // '#title' => t('Field wrapper'),
        '#description' => t('Enter HTML tag name of the wrapper element.'),
        '#default_value' => $plugin->getThirdPartySetting('flexible_field_wrappers', 'field_item_wrapper_other'),
        '#element_validate' => [ [self::class, 'validateHtmlTagName'] ],
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $field_name . '][settings_edit_form][third_party_settings][flexible_field_wrappers][field_item_wrapper]"]' => ['value' => 'other'],
          ],
        ],
        '#depended_field' => 'field_item_wrapper'
      ];

      $elements['field_wrappers']['field_item_wrapper_href'] = [
        '#type' => 'textfield',
        '#title' => t('Link destination'),
        '#description' => t('Enter destination for the link. Tokens are supported.'),
        '#default_value' => $plugin->getThirdPartySetting('flexible_field_wrappers', 'field_item_wrapper_href'),
        '#element_validate' => [ [self::class, 'validateHref'] ],
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $field_name . '][settings_edit_form][third_party_settings][flexible_field_wrappers][field_item_wrapper]"]' => ['value' => 'link'],
          ],
        ],
        '#depended_field' => 'field_item_wrapper'
      ];

      if (\Drupal::moduleHandler()->moduleExists('token')) {
        $elements['field_wrappers']['field_item_wrapper_href_token'] = [
          '#type' => 'item',
          '#theme' => 'token_tree_link',
          '#token_types' => $token_types,
          '#states' => [
            'visible' => [
              ':input[name="fields[' . $field_name . '][settings_edit_form][third_party_settings][flexible_field_wrappers][field_item_wrapper]"]' => ['value' => 'link'],
            ],
          ],
        ];
      }

      $elements['field_wrappers']['field_item_wrapper_classes'] = [
        '#type' => 'textfield',
        '#title' => t('Field item wrapper HTML classes'),
        '#description' => t('Enter one or more HTML class names separated by spaces. It can be used to style the field items with CSS.'),
        '#default_value' => $plugin->getThirdPartySetting('flexible_field_wrappers', 'field_item_wrapper_classes'),
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $field_name . '][settings_edit_form][third_party_settings][flexible_field_wrappers][field_item_wrapper]"]' => ['!value' => 'none'],
          ],
        ],
      ];

      $elements['field_wrappers']['field_item_wrapper_attributes'] = [
        '#title' => t('Field item wrapper attributes'),
        '#type' => 'textarea',
        '#description' => t('Set attributes for each field item wrapper. Enter one value per line, in the format attribute|value. The value is optional. Tokens are supported in attribute values.'),
        '#default_value' => $plugin->getThirdPartySetting('flexible_field_wrappers', 'field_item_wrapper_attributes'),
        '#element_validate' => [ [self::class, 'validateHtmlAttributesList'] ],
        '#depended_field' => 'field_item_wrapper',
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $field_name . '][settings_edit_form][third_party_settings][flexible_field_wrappers][field_item_wrapper]"]' => ['!value' => 'none'],
          ],
        ],
      ];
      if (\Drupal::moduleHandler()->moduleExists('token')) {
        $elements['field_wrappers']['field_item_wrapper_attributes_token'] = [
          '#type' => 'item',
          '#theme' => 'token_tree_link',
          '#token_types' => $token_types,
          '#states' => [
            'visible' => [
              ':input[name="fields[' . $field_name . '][settings_edit_form][third_party_settings][flexible_field_wrappers][field_item_wrapper]"]' => ['!value' => 'none'],
            ],
          ],
        ];
      }
    }

    return $elements['field_wrappers'];
  }

  public static function validateHtmlTagName(&$element, FormStateInterface $form_state, &$complete_form) {
    $input_exists = FALSE;
    $input = NestedArray::getValue($form_state->getValues(), $element['#parents'], $input_exists);
  
    $parents = $element['#parents'];
    array_pop($parents);
    $parents = array_merge($parents, [$element['#depended_field']]);
    $select_field_value = NestedArray::getValue($form_state->getValues(), $parents);
  
    if ($select_field_value == 'other' && empty($input)) {
      $form_state->setError($element, t('Please enter a wrapper tag name.'));
    }
    else if (!empty($input) && !preg_match('/^[a-zA-Z]+\d?$/', $input)) {
      $form_state->setError($element, t('Enter a valid HTML tag name. %name is not a valid tag name.', ['%name' => $input]));
    }
  }

  public static function validateHtmlAttributesList(&$element, FormStateInterface $form_state, &$complete_form) {
    $input_exists = FALSE;
    $input = NestedArray::getValue($form_state->getValues(), $element['#parents'], $input_exists);
  
    $list = explode("\n", $input);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');
  
    $generated_keys = $explicit_keys = FALSE;
    foreach ($list as $position => $text) {
      // Check for an explicit key.
      $matches = [];
      if (preg_match('/^([\w]+(\-[\w]+)*)\|(.*)$/', $text, $matches)) {
        // Trim key and value to avoid unwanted spaces issues.
        $key = trim($matches[1]);
        $value = trim($matches[3]);
      }
      else {
        $form_state->setError($element, t('Invalid attribute name.'));
        break;
      }
    }
    // return $values;
  }

  public static function validateHref(&$element, FormStateInterface $form_state, &$complete_form) {
    $input_exists = FALSE;
    $input = NestedArray::getValue($form_state->getValues(), $element['#parents'], $input_exists);

    $parents = $element['#parents'];
    array_pop($parents);
    $parents = array_merge($parents, [$element['#depended_field']]);
    $select_field_value = NestedArray::getValue($form_state->getValues(), $parents);

    if ($select_field_value == 'link' && empty($input)) {
      $form_state->setError($element, t('Please enter a destination for the link.'));
    }
  }

  public static function formatter_settings_summary_alter(array &$summary, array $context) {
    $formatter_plugin = $context['formatter'];
    $field_wrapper = $formatter_plugin->getThirdPartySetting('flexible_field_wrappers', 'field_wrapper');
    if (!empty($field_wrapper) && $field_wrapper != 'none') {
      if ($field_wrapper != 'other') {
        $summary[] = t('Field wrapper: @wrapper', ['@wrapper' => $field_wrapper]);
      }
      else {
        $summary[] = t('Field wrapper: @wrapper', ['@wrapper' => $formatter_plugin->getThirdPartySetting('flexible_field_wrappers', 'field_wrapper_other')]);
      }
    }

    $field_wrapper_classes = $formatter_plugin->getThirdPartySetting('flexible_field_wrappers', 'field_wrapper_classes');
    if (!empty($field_wrapper_classes)) {
      $summary[] = t('Field wrapper classes: @classes', ['@classes' => implode(', ', explode(' ', $field_wrapper_classes))]);
    }

    $field_item_wrapper = $formatter_plugin->getThirdPartySetting('flexible_field_wrappers', 'field_item_wrapper');
    if (!empty($field_item_wrapper) && $field_item_wrapper != 'none') {
      if ($field_item_wrapper != 'other') {
        $summary[] = t('Field item wrapper: @wrapper', ['@wrapper' => $field_item_wrapper]);
      }
      else {
        $summary[] = t('Field item wrapper: @wrapper', ['@wrapper' => $formatter_plugin->getThirdPartySetting('flexible_field_wrappers', 'field_item_wrapper_other')]);
      }
    }

    $field_item_wrapper_classes = $formatter_plugin->getThirdPartySetting('flexible_field_wrappers', 'field_item_wrapper_classes');
    if (!empty($field_item_wrapper_classes)) {
      $summary[] = t('Field wrapper classes: @classes', ['@classes' => implode(', ', explode(' ', $field_item_wrapper_classes))]);
    }
  }

  public static function getAttributesFromConfig($attributes_string, $token_data) {
    $attribute_list = explode("\n", $attributes_string);
    $attribute_list = array_map('trim', $attribute_list);
    $attribute_list = array_filter($attribute_list, 'strlen');

    $attributes = [];
    foreach ($attribute_list as $text) {
      // Check for an explicit key.
      $matches = [];
      if (preg_match('/^([\w]+(\-[\w]+)*)\|(.*)$/', $text, $matches)) {
        // Trim key and value to avoid unwanted spaces issues.
        $key = trim($matches[1]);
        $value = trim($matches[3]);
        $attributes[$key] = \Drupal::token()->replace($value, $token_data);
      }
    }
    return $attributes;
  }

  public static function applyWrappersToField(&$field_content, $settings, EntityInterface $entity) {
    if (isset($settings['field_wrapper'])) {
      $token_data = [$entity->getEntityTypeId() => $entity];
      $field_wrapper = $settings['field_wrapper'];
      if ($field_wrapper != 'none') {
        $attributes = FieldWrapper::getAttributesFromConfig($settings['field_wrapper_attributes'] ?? '', $token_data);
        if ($field_wrapper != 'other') {
          $tag = $field_wrapper;
        }
        else {
          $tag = $settings['field_wrapper_other'];
        }
        if (!isset($field_content['#theme_wrappers'])) {
          $field_content['#theme_wrappers'] = [];
        }
        $field_wrapper_classes = $settings['field_wrapper_classes'] ?? '';
        if (!empty($field_wrapper_classes)) {
          $field_wrapper_classes = explode(' ', $field_wrapper_classes);
        }
        else {
          $field_wrapper_classes = [];
        }

        $field_content['#theme_wrappers']['field_wrapper'] = [
          '#attributes' => [
            'class' => array_merge(['field-wrapper'], $field_wrapper_classes),
          ] + $attributes,
          '#wrapper_tag' => $tag,
        ];
      }
    }

    if (isset($settings['field_item_wrapper'])) {
      $field_item_wrapper = $settings['field_item_wrapper'];
      if ($field_item_wrapper != 'none') {
        $attributes = FieldWrapper::getAttributesFromConfig($settings['field_item_wrapper_attributes'] ?? '', $token_data);

        if ($field_item_wrapper == 'link') {
          $tag = 'a';
          $attributes['href'] = \Drupal::token()->replace($settings['field_item_wrapper_href'], $token_data);
        }
        else if ($field_item_wrapper != 'other') {
          $tag = $field_item_wrapper;
        }
        else {
          $tag = $settings['field_item_wrapper_other'];
        }
        foreach (Element::children($field_content) as $child_key) {
          if (!isset($field_content[$child_key]['#theme_wrappers'])) {
            $field_content[$child_key]['#theme_wrappers'] = [];
          }
          $field_item_wrapper_classes = $settings['field_item_wrapper_classes'] ?? '';
          if (!empty($field_item_wrapper_classes)) {
            $field_item_wrapper_classes = explode(' ', $field_item_wrapper_classes);
          }
          else {
            $field_item_wrapper_classes = [];
          }

          $field_content[$child_key]['#theme_wrappers']['field_item_wrapper'] = [
            '#attributes' => [
              'class' => array_merge(['field-item-wrapper'], $field_item_wrapper_classes),
            ] + $attributes,
            '#wrapper_tag' => $tag,
          ];
        }
      }
    }
  }
}
